const headerBurgerBtn = document.querySelector(".header__burger-button");
const headerBtnClose = document.querySelector(".header__button-close");
const headerMenuList = document.querySelector(".header__dropdown-menu");

document.querySelector(".header__nav-menu-btn").addEventListener("click",event => {
        let target = event.target;

        if (target.classList.contains("header__burger-button")) {
            headerBurgerBtn.classList.remove("show");
            headerBurgerBtn.classList.add("hide");
            headerMenuList.classList.remove("hide");
            headerMenuList.classList.add ("show");
            headerBtnClose.classList.remove("hide");
            headerBtnClose.classList.add("show");
        }
        else if (target.classList.contains("header__button-close")) {
            headerBtnClose.classList.remove("show");
            headerBtnClose.classList.add("hide");
            headerMenuList.classList.remove("show");
            headerMenuList.classList.add ("hide");
            headerBurgerBtn.classList.remove("hide");
            headerBurgerBtn.classList.add("show");
        }
}
)
