import gulp from "gulp";
import concat from 'gulp-concat';
import terser from 'gulp-terser';
import cleancss from 'gulp-clean-css';
import imagemin from 'gulp-imagemin';
import browserSync from 'browser-sync';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import clean from 'gulp-clean';
import autoprefixer from 'gulp-autoprefixer';

const bS = browserSync.create();
const sass = gulpSass(dartSass);

const buildStyles = () => gulp.src('./src/styles/**/*.scss')
    .pipe(sass())
    .pipe((autoprefixer()))
    .pipe(concat('styles.min.css'))
    .pipe(cleancss())
    .pipe(gulp.dest('./dist/'));

const js = () => gulp.src("./src/js/**/*.js")
    .pipe(concat('script.min.js'))
    .pipe(terser())
    .pipe(gulp.dest('./dist/'));

const minImage = () => gulp.src("./src/img/**/*")
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/img'));

const cleanDist = () => gulp.src("./dist/")
    .pipe(clean());

export const build = gulp.series(cleanDist, gulp.parallel(buildStyles, js, minImage));

export const dev = gulp.series(build, () => {
    bS.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch(['./src/**/*', './index.html'], gulp.series(build, (done) => {
        bS.reload();
        done();
    }))
});




